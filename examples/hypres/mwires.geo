// This code was created by pygmsh v4.1.2.
SetFactory("OpenCASCADE");
Mesh.CharacteristicLengthMin = 0.1;
Mesh.CharacteristicLengthMax = 0.1;
Geometry.NumSubEdges = 100;
p0 = newp;
Point(p0) = {0.1, 0.9, 0.3486784401000001, 0.3};
p1 = newp;
Point(p1) = {0.5, 0.3, 5.9048999999999975e-06, 0.3};
p2 = newp;
Point(p2) = {1.0, 0.0, 0.0, 0.3};
p3 = newp;
Point(p3) = {1.5, 0.3, 5.9048999999999975e-06, 0.3};
p4 = newp;
Point(p4) = {1.9, 0.9, 0.3486784401000001, 0.3};
l0 = newl;
Line(l0) = {p0, p4};
l1 = newl;
Spline(l1) = {p0, p1, p2, p3, p4};
ll0 = newll;
Line Loop(ll0) = {l1, l0};
Surface(1) = {ll0};
p5 = newp;
Point(p5) = {0.15, 0.9, -0.25, 0.3};
p6 = newp;
Point(p6) = {0.5, 0.2, -0.25, 0.3};
p7 = newp;
Point(p7) = {1.0, -0.3, -0.25, 0.3};
p8 = newp;
Point(p8) = {1.5, 0.2, -0.25, 0.3};
p9 = newp;
Point(p9) = {1.85, 0.9, -0.25, 0.3};
l2 = newl;
Line(l2) = {p5, p9};
l3 = newl;
Spline(l3) = {p5, p6, p7, p8, p9};
ll1 = newll;
Line Loop(ll1) = {l3, l2};
p10 = newp;
Point(p10) = {0.9, 0.8, -0.5, 0.3};
p11 = newp;
Point(p11) = {0.8, 0.5, -0.5, 0.3};
p12 = newp;
Point(p12) = {1.0, -0.2, -0.5, 0.3};
p13 = newp;
Point(p13) = {1.2, 0.5, -0.5, 0.3};
p14 = newp;
Point(p14) = {1.1, 0.8, -0.5, 0.3};
l4 = newl;
Line(l4) = {p10, p14};
l5 = newl;
Spline(l5) = {p10, p11, p12, p13, p14};
ll2 = newll;
Line Loop(ll2) = {l5, l4};
Surface(3) = {ll2};
ThruSections(1) = {ll0,ll1,ll2};