// This code was created by pygmsh v4.1.1.
SetFactory("OpenCASCADE");
p0 = newp;
Point(p0) = {0.0, 0.0, 0.0, 0.3};
p1 = newp;
Point(p1) = {1.0, 0.0, 0.0, 0.3};
p2 = newp;
Point(p2) = {1.0, 1.0, 0.0, 0.3};
p3 = newp;
Point(p3) = {0.0, 1.0, 0.0, 0.3};
p4 = newp;
Point(p4) = {0.0, 0.0, 1.0, 0.3};
p5 = newp;
Point(p5) = {1.0, 0.0, 1.0, 0.3};
p6 = newp;
Point(p6) = {1.0, 1.0, 1.0, 0.3};
p7 = newp;
Point(p7) = {0.0, 1.0, 1.0, 0.3};
l0 = newl;
Line(l0) = {p0, p1};
l1 = newl;
Line(l1) = {p1, p2};
l2 = newl;
Line(l2) = {p2, p3};
l3 = newl;
Line(l3) = {p3, p0};
l4 = newl;
Line(l4) = {p4, p5};
l5 = newl;
Line(l5) = {p5, p6};
l6 = newl;
Line(l6) = {p6, p7};
l7 = newl;
Line(l7) = {p7, p4};
ll0 = newll;
Line Loop(ll0) = {l0, l1, l2, l3};
ll1 = newll;
Line Loop(ll1) = {l4, l5, l6, l7};
s0 = news;
Plane Surface(s0) = {ll1};
s1 = news;
Plane Surface(s1) = {ll0};

l_wire[] = Abs(Boundary{Surface{s1};});
Delete{ Surface{s1}; }

l_wire1[] = Abs(Boundary{Surface{s0};});
Delete{ Surface{s0}; }

ThruSections(1) = {ll0,ll1};







