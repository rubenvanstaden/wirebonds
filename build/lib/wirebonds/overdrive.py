"""

Usage:
    wirebonds --cell=<cellname> --config=<configname>
    wirebonds (-h | --help)
    wirebonds (-V | --version)

Options:
    -h --help     show this screen.
    -V --version  show version.
    --verbose     print more text
    --verbosemesh set the verbose of gmsh to True
    --layers      list the layers to mesh

"""


import os
import json
import gdsyuna
import pygmsh
import meshio
import numpy as np
import yuna
import math

from docopt import docopt
from wirebonds import tools
from wirebonds import version

from pygmsh.opencascade.surface_base import SurfaceBase
from pygmsh.opencascade.volume_base import VolumeBase
from pygmsh.built_in.volume import Volume


nm_units = 10e-6


def remove_surface(geom, _id):
    abs_string = 'l_wire[] = Abs(Boundary{Surface{' + str(_id) + '};});'
    delete_string = 'Delete{ Surface{' + str(_id) + '}; }'

    geom.add_raw_code(abs_string)
    geom.add_raw_code(delete_string)


def top_spline(geom, z):
    lcar=0.3

    points = []

    # for i in range(len(x)):
    #     pp = geom.add_point([x[i], y[i], 0.0], lcar)
    #     points.append(pp)

    # for x in xp_1:
    #     y = math.exp(x)
    #     pp = geom.add_point([x, y, 0.0], lcar)
    #     points.append(pp)
    #
    # for x in xp_2:
    #     y = x**2
    #     pp = geom.add_point([x, y, 0.0], lcar)
    #     points.append(pp)

    # s1 = geom.add_spline(points)

    x = [0.1, 0.5, 1.0, 1.5, 1.9]
    y = [0.9, 0.3, 0.0, 0.3, 0.9]

    # p1 = geom.add_point([0.1, 0.9, 0.0], lcar)
    # p2 = geom.add_point([0.5, 0.3, 0.0], lcar)
    # p3 = geom.add_point([1.0, 0.0, 0.0], lcar)
    # p4 = geom.add_point([1.5, 0.3, 0.0], lcar)
    # p5 = geom.add_point([1.9, 0.9, 0.0], lcar)

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z + y[i]**10], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    # ss = geom.add_plane_surface(ll)

    # remove_surface(geom, poly_1.surface.id)
    # remove_surface(geom, poly_2.surface.id)
    #
    # thru_string = 'ThruSections(1) = {' + str(poly_1.line_loop.id) + ',' + str(poly_2.line_loop.id) + '};'
    #
    # geom.add_raw_code(thru_string)

    return ll


def mid_spline(geom, z):

    lcar=0.3

    points = []
    x = [0.15, 0.5, 1.0, 1.5, 1.85]
    y = [0.9, 0.2, -0.3, 0.2, 0.9]

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    # ss = geom.add_plane_surface(ll)

    # remove_surface(geom, poly_1.surface.id)
    # remove_surface(geom, poly_2.surface.id)

    # thru_string = 'ThruSections(1) = {' + str(poly_1.line_loop.id) + ',' + str(poly_2.line_loop.id) + '};'

    # geom.add_raw_code(thru_string)

    return ll


def bot_spline(geom, z):

    lcar=0.3

    points = []
    x = [0.9, 0.8, 1.0, 1.2, 1.1]
    y = [0.8, 0.5, -0.2, 0.5, 0.8]

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    # ss = geom.add_plane_surface(ll)

    # remove_surface(geom, poly_1.surface.id)
    # remove_surface(geom, poly_2.surface.id)

    # thru_string = 'ThruSections(1) = {' + str(poly_1.line_loop.id) + ',' + str(poly_2.line_loop.id) + '};'

    # geom.add_raw_code(thru_string)

    return ll


def connection_circle(geom):
    d0 = geom.add_disk([0.9, 1.0, -0.25], 0.9)
    geom.add_raw_code('Rotate {{1, 0, 0}, {1.0, 0.9, 0}, Pi/2} { Surface{s0}; }')


def circle_compress(geom):
    cylinder = geom.add_cylinder(
        [0.0, 0.0, 0.0], [0.0, 0.0, 1.5],
        0.5, char_length=0.1
    )

    ball = geom.add_ball(
        [-1.5, 0.0, 1.3], 1.5, x0=None, x1=None,
        alpha=1.0*np.pi, char_length=0.25
    )

    geom.boolean_difference([cylinder], [ball])


def add_cascade_circle(geom, _id, x0, radius, angle):
    ii = 'l{}'.format(_id)

    args = list(x0) + [radius]
    if angle is not None:
        args.append(angle)
    args = ', '.join(['{}'.format(arg) for arg in args])

    circle = '\n'.join([
        '{} = newl;'.format(ii),
        'Circle({}) = {{{}}};'.format(ii, args)
    ])

    geom.add_raw_code(circle)


def add_cascade_line_loop(geom, _id, line_id):
    ii = 'll{}'.format(_id)

    line_loop = '\n'.join([
        '{} = newll;'.format(ii),
        'Line Loop({}) = {{{}}};'.format(ii, line_id)
    ])

    geom.add_raw_code(line_loop)


def circle_function(geom, i):

    num_turns = 1
    h = 1 * num_turns
    num_points = 20

    theta = i * 0.5 * np.pi/num_points

    x = np.cos(-theta)
    y = np.sin(-theta)
    z = 0

    return [x, y, z]


def corner_function(geom, i):
    x = 0.0
    y = 1.0
    z = 0.2 * i

    return [x, y, z]

    # spline_points, points = [], []
    #
    # for i in range(num_points):
    #     # x = 0.0
    #     # y = 1.0
    #     # z = i * 0.2
    #
    #     x = 0.0
    #     y = 1.0
    #     z = 0.2 * i
    #
    #     # if i < 5:
    #     #     x = 0.0
    #     #     y = 1.0
    #     #     z = i * 0.2
    #     # else:
    #     #     x = i * 0.2
    #     #     y = 1.0
    #     #     z = 1.0
    #
    #     p = [x, y, z]
    #
    #     sp = geom.add_point(p, lcar=0.1)
    #
    #     spline_points.append(sp)
    #     points.append(p)
    #
    # return spline_points, points


def rotate_surface(geom, points, sid, axis):

    ax = '{{{}}}'.format(','.join([str(v) for v in axis]))
    pp = '{{{}}}'.format(','.join([str(v) for v in points]))

    # line = '{}{{{}}};'.format('Line', 'l{}'.format(sid+1))
    # surface = '{}{{{}}};'.format('Surface', 's{}'.format(sid))
    surface = '{}{{{}}};'.format('Surface', '{}'.format(sid))

    rotate = ''.join([
        'Rotate {{{}, {}, {}}} '.format(pp, ax, 'Pi/2'),
        # '{{{}}}'.format(line)
        '{{{}}}'.format(surface)
    ])

    geom.add_raw_code(rotate)







def extrude_wire(geom, points, coords, wire_id):
    rd = 0.1
    r = 0.38

    ss1 = geom.add_spline(points)

    ww = geom.add_wire([ss1])
    dd = geom.add_disk(coords[0], rd)

    # rotate_surface(geom, [0, -r, 0], dd.id, coords[0])
    # rotate_surface(geom, coords[0], dd.id, [0,0,0])
    geom.rotate_surface(coords[0], [0, 0, 0], np.pi/2, dd.id)

    geom.extrude(dd, using_wire=ww.id)
    # add_extrude_wire(geom, 0, 'l1')


def add_curved_wire(geom):
    npts = 20

    points, coords = [], []

    for i in range(npts + 1):
        p = circle_function(geom, i)

        sp = geom.add_point(p, lcar=0.1)

        coords.append(p)
        points.append(sp)

    extrude_wire(geom, points, coords, 1)

    return coords, points


def add_straight_wire(geom, mc, mp):
    npts = 20
    lh = 1.49

    coords, points = [], []

    x = mc[npts][0] - lh
    y = mc[npts][1]
    z = mc[npts][2]

    pp = geom.add_point([x, y, z], lcar=0.1)

    coords.append(mc[npts])
    coords.append([x, y, z])

    points.append(mp[npts])
    points.append(pp)

    extrude_wire(geom, points, coords, 2)

    return coords, points


def translate_element(geom, x0, point):
    duplicata = 'Duplicata {{ Point{{{}}}; }}'.format(point)

    translate = ''.join([
        'new_points[] += Translate {{{}, {}, {}}} '.format(x0[0], x0[1], x0[2]),
        '{{{}}};'.format(duplicata)
    ])

    geom.add_raw_code(translate)


def duplicate_element(geom, N):
    for i in range(2, N):
        func_a = 'l~{{{}}}()'.format(i-1)
        func_b = 'l~{{{}}}()'.format(i)

        duplicata = 'Duplicata {{ Line{{{}}}; }}'.format(func_a)
        line = 'Line{{{}}};'.format(func_b)

        if i != N-1:
            translate = ''.join([
                'l~{{{}}}() = Translate {{{}, {}, {}}} '.format(i, 0, 0, 3/(N-1)),
                '{{{}}};'.format(duplicata)
            ])
            geom.add_raw_code(translate)
        else:
            # Rotate {{0, 0, 1}, {0, 0, 0}, angle/(N-1)} { Curve{l~{i}()}; }
            translate = ''.join([
                'l~{{{}}}() = Translate {{{}, {}, {}}} '.format(i, 3/(N-1), 0, 3),
                '{{{}}};'.format(duplicata)
            ])

            ax = '{{{}}}'.format(','.join([str(v) for v in [0,1,0]]))
            pp = '{{{}}}'.format(','.join([str(v) for v in [0,0,3]]))

            rotate = ''.join([
                'Rotate {{{}, {}, {}}} '.format(ax, pp, 'Pi/2'),
                '{{ {} }}'.format(line)
            ])

            geom.add_raw_code(translate)
            geom.add_raw_code(rotate)

        line_loop = 'Line Loop({}) = l~{{{}}}();'.format(i, i)

        print(translate)
        print(line_loop)

        # geom.add_raw_code(translate)
        geom.add_raw_code(line_loop)

        # geom.add_raw_code('l~{i}() = Translate{0,0,1/(N-1)}{ Duplicata{ Line{l~{i-1}()}; } };')
        # geom.add_raw_code('Line Loop(i) = l~{i}();')


def create_ellipse_wire(geom):
    a = 0.300;
    b = 0.300;
    L = 0.5;

    lc1 = 0.100;
    lc2 = 0.050;
    lc3 = 0.025;

    angle_cos = np.cos(np.pi/6)
    angle_sin = np.sin(np.pi/6)

    p0 = geom.add_point([a*angle_cos, a*angle_sin, 0], lc2)
    p1 = geom.add_point([-b*angle_sin, b*angle_cos, 0], lc2)
    p2 = geom.add_point([-a*angle_cos, -a*angle_sin, 0], lc2)
    p3 = geom.add_point([b*angle_sin, -b*angle_cos, 0], lc2)
    p4 = geom.add_point([0, 0, 0], lc2)

    geom.add_ellipse_arc(p2, p4, p1, p1)
    geom.add_ellipse_arc(p2, p4, p3, p3)
    geom.add_ellipse_arc(p0, p4, p3, p3)
    geom.add_ellipse_arc(p0, p4, p1, p1)

    geom.add_raw_code('l~{1}() = {l0:l3};')
    geom.add_raw_code('Line Loop(1) = l~{1}();')

    duplicate_element(geom, 5)

    # rotate_surface(geom, points, 9, [0, 1, 0])

    geom.add_raw_code('ThruSections(1) = {1:4};')


# def add_extrude_wire(geom, s_id, l_id):
#     ss = 's{}'.format(s_id)
#     ll = '{}'.format(l_id)
#
#     surface = 'Surface{{ {} }};'.format(ss)
#
#     extrude = 'Extrude {{ {} }} Using Wire {{ {} }};'.format(surface, ll)
#
#     geom.add_raw_code(extrude)


def create_bend(geom):

    npts = 16
    r = 0.38
    rd = 0.125
    mes = 1e-2
    lh = 1.49
    lv1 = 0.74
    lv2 = 2.07

    spline_points = []

    for i in range(npts+1):
        theta = i * 0.5 * np.pi/npts
        sp = geom.add_point([r * np.cos(-theta), r * np.sin(-theta), 0], mes)
        spline_points.append(sp)

    spl_1 = geom.add_spline(spline_points)
    add_wire(geom, 1, spl_1.id)

    s_id = 0

    pp = [0, -r, 0]
    geom.add_disk(pp, rd)
    rotate_surface(geom, pp, s_id, [0, 0, 0])

    extrude_wire(geom, s_id, 'l1')


def create_wirebond():
    """ Read in the layers from the GDS file,
    do clipping and send polygons to
    GMSH to generate the Mesh. """

    args = docopt(__doc__, version=version.__version__)
    tools.cyan_print('Generating wirebonds...')
    tools.parameter_print(args)

    geom = pygmsh.opencascade.Geometry()

    geom.add_raw_code('Mesh.CharacteristicLengthMin = 0.1;')
    geom.add_raw_code('Mesh.CharacteristicLengthMax = 0.1;')
    geom.add_raw_code('Geometry.NumSubEdges = 100;')

    # ll1 = top_spline(geom, 0.0)
    # ll2 = mid_spline(geom, -0.25)
    # ll3 = bot_spline(geom, -0.5)

    # connection_circle(geom)
    # circle_compress(geom)
    # create_ellipse_wire(geom)
    # using_wire_object(geom)

    # wirebond(geom)
    # create_bend(geom)

    coords, points = add_curved_wire(geom)
    coords, points = add_straight_wire(geom, coords, points)

    imesh = pygmsh.generate_mesh(geom, geo_filename='mwires.geo')

    meshio.write('wirebonds.vtk', *imesh)

    # meshio.write('surotto.vtk', *generate_mesh())
    # meshio.write('box.vtk', *diff_boxes())
    # meshio.write('extrude.vtk', *extrude_polygon())

    tools.cyan_print('Wirebonds completed.\n')
