"""

Usage:
    wirebonds --cell=<cellname> --config=<configname>
    wirebonds (-h | --help)
    wirebonds (-V | --version)

Options:
    -h --help     show this screen.
    -V --version  show version.
    --verbose     print more text
    --verbosemesh set the verbose of gmsh to True
    --layers      list the layers to mesh

"""


import os
import json
import gdsyuna
import pygmsh
import meshio
import numpy as np
import yuna
import math

from docopt import docopt
from wirebonds import tools
from wirebonds import version

from pygmsh.opencascade.surface_base import SurfaceBase
from pygmsh.opencascade.volume_base import VolumeBase
from pygmsh.built_in.volume import Volume


nm_units = 10e-6


def remove_surface(geom, _id):
    abs_string = 'l_wire[] = Abs(Boundary{Surface{' + str(_id) + '};});'
    delete_string = 'Delete{ Surface{' + str(_id) + '}; }'

    geom.add_raw_code(abs_string)
    geom.add_raw_code(delete_string)


def add_opencascade_surface(geom, sid, _id):
    surface = 'Surface({}) = {{{}}};'.format(sid, _id)
    geom.add_raw_code(surface)


def top_spline(geom, z):
    lcar=0.3

    points = []

    x = [0.1, 0.5, 1.0, 1.5, 1.9]
    y = [0.9, 0.3, 0.0, 0.3, 0.9]

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z + y[i]**10], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    add_opencascade_surface(geom, 1, ll.id)

    return ll


def mid_spline(geom, z):

    lcar=0.3

    points = []
    x = [0.15, 0.5, 1.0, 1.5, 1.85]
    y = [0.9, 0.2, -0.3, 0.2, 0.9]

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    # add_opencascade_surface(geom, 2, ll.id)

    return ll


def bot_spline(geom, z):

    lcar=0.3

    points = []
    x = [0.9, 0.8, 1.0, 1.2, 1.1]
    y = [0.8, 0.5, -0.2, 0.5, 0.8]

    for i in range(len(x)):
        pp = geom.add_point([x[i], y[i], z], lcar)
        points.append(pp)

    l1 = geom.add_line(points[0], points[len(points)-1])
    s1 = geom.add_spline(points)

    ll = geom.add_line_loop([s1, l1])
    add_opencascade_surface(geom, 3, ll.id)

    return ll


def f0_circle(geom, i):

    num_turns = 1
    h = 1 * num_turns
    num_points = 20

    theta = i * 0.5 * np.pi/num_points

    x = np.cos(-theta)
    y = np.sin(-theta)
    z = 0

    return [x, y, z]


def f1_circle(geom, i):

    num_turns = 1
    h = 1 * num_turns
    num_points = 20

    theta = i * 0.5 * np.pi/num_points

    x = -np.cos(-theta)
    y = np.sin(-theta)
    z = 0

    return [x, y, z]


def extrude_wire(geom, points, coords, bp):
    rd = 0.1
    r = 0.38

    ss1 = geom.add_spline(points)

    ww = geom.add_wire([ss1])
    dd = geom.add_disk(coords[0], rd)

    geom.rotate_surface(coords[bp], [0, 0, 0], np.pi/2, dd.id)

    geom.extrude(dd, using_wire=ww.id)


def add_curved_wire(geom, mc, mp, endpoint, func):
    npts = 20

    coords, points = [], []

    for i in range(npts + 1):
        p = None
        if func == 0:
            p = f0_circle(geom, i)
        elif func == 1:
            p = f1_circle(geom, i)

        if p is not None:
            p[0] = endpoint[0] + p[0]
            p[1] = endpoint[1] + p[1]
            p[2] = endpoint[2] + p[2]

            sp = geom.add_point(p, lcar=0.1)

            coords.append(p)
            points.append(sp)
        else:
            raise ValueError('p cannot be None')

    extrude_wire(geom, points, coords, 0)

    return coords, points


def add_straight_wire(geom, mc, mp, endpoint):
    npts = 20

    coords, points = [], []

    x = endpoint[0] + mc[npts][0]
    y = endpoint[1] + mc[npts][1]
    z = endpoint[2] + mc[npts][2]

    pp = geom.add_point([x, y, z], lcar=0.1)

    coords.append(mc[npts])
    coordsappend([x, y, z])

    points.append(mp[npts])
    points.append(pp)

    extrude_wire(geom, points, coords, 0)

    return coords, points


def generate_begin_contact(geom):
    pass


def generate_base_wire(geom):
    endpoint = [0.0, 0.0, 0.0]
    coords, points = add_curved_wire(geom, [], [], endpoint, 0)

    coords, points = add_straight_wire(geom, coords, points, [-1.49, 0, 0])

    endpoint = [-1.49, 0.0, 0.0]
    coords, points = add_curved_wire(geom, coords, points, endpoint, 1)


def generate_end_contact(geom):
    ll1 = top_spline(geom, 0.0)
    ll2 = mid_spline(geom, -0.25)
    ll3 = bot_spline(geom, -0.5)

    thru = 'ThruSections({}) = {{{}}};'.format(1, 'll0,ll1,ll2')
    geom.add_raw_code(thru)


def create_wirebond():
    """ Read in the layers from the GDS file,
    do clipping and send polygons to
    GMSH to generate the Mesh. """

    args = docopt(__doc__, version=version.__version__)
    tools.cyan_print('Generating wirebonds...')
    tools.parameter_print(args)

    geom = pygmsh.opencascade.Geometry()

    geom.add_raw_code('Mesh.CharacteristicLengthMin = 0.1;')
    geom.add_raw_code('Mesh.CharacteristicLengthMax = 0.1;')
    geom.add_raw_code('Geometry.NumSubEdges = 100;')

    # generate_begin_contact(geom)
    # generate_base_wire(geom)
    generate_end_contact(geom)

    imesh = pygmsh.generate_mesh(geom, geo_filename='mwires.geo')

    meshio.write('wirebonds.vtk', *imesh)

    tools.cyan_print('Wirebonds completed.\n')
